# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
import operator

from trytond.i18n import gettext
from trytond.exceptions import UserWarning, UserError
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction


class PurchaseLine(metaclass=PoolMeta):
    __name__ = 'purchase.line'

    def get_invoice_line(self):
        records = super(PurchaseLine, self).get_invoice_line()
        for rec in records:
            account_stock_used = self.product.account_stock_used
            if self.product.template.type == 'goods' and account_stock_used:
                rec.account = account_stock_used.id
        return records


class InvoiceLine(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'

    @classmethod
    def __setup__(cls):
        super(InvoiceLine, cls).__setup__()

    # @staticmethod
    # def _account_domain(type_): 
    #     # This method overload original adding stock type to "in" domain
    #     if type_ == 'out':
    #         return ['OR', ('type.revenue', '=', True)]
    #     elif type_ == 'in':
    #         return ['OR',
    #                 ('type.expense', '=', True),
    #                 ('type.debt', '=', True),
    #                 ('type.stock', '=', True),
    #                 ]

    def _get_latin_move_lines(self, amount, type_, account_stock_method):
        '''
        Return account move for latin stock accounting
        '''
        self.amount = abs(self.amount)
        MoveLine = Pool().get('account.move.line')

        assert type_.startswith('in_') or type_.startswith('out_'), \
            'wrong type'
        result = []
        move_line = MoveLine()
        desc = self.product.name if self.product else self.description
        move_line.description = desc
        move_line.amount_second_currency = None
        move_line.second_currency = None
        account_type = 'account_stock'
        if type_ == 'in_customer':
            move_line.debit = amount
            move_line.credit = Decimal('0.0')
        else:
            move_line.debit = Decimal('0.0')
            move_line.credit = amount
        account = getattr(self.product.account_category, account_type)
        account_stock_out = getattr(
            self.product.account_category, 'account_stock_out')
        if account_stock_out:
            account = account_stock_out
        move_line.account = account
        if not move_line.account:
            raise UserError(gettext(
                'account_stock_latin.msg_missing_account_stock',
                product=self.product.name
            ))
        if move_line.account.party_required:
            move_line.party = self.invoice.party

        result.append(move_line)
        debit, credit = move_line.debit, move_line.credit

        move_line = MoveLine()

        move_line.description = self.description
        move_line.amount_second_currency = move_line.second_currency = None
        move_line.debit, move_line.credit = credit, debit

        move_line.account = self.product.account_cogs_used
        if not move_line.account:
            raise UserError(gettext(
                'account_stock_latin.msg_missing_account_cogs_used',
                product=self.product.name
            ))
        if move_line.account.party_required:
            move_line.party = self.invoice.party

        result.append(move_line)
        return result

    # def _temporal_fix(self, result):
    #     # Change account expense by stock based in category, just to 31-12-2021
    #     for ln in result:
    #         acc = self.product.account_stock_used
    #         if self.quantity < 0 and self.product.account_category.account_return_purchase:
    #             acc = self.product.account_category.account_return_purchase
    #         if acc and ln.account != acc:
    #             ln.account = acc
    #     return result

    def get_move_lines(self):
        pool = Pool()
        Move = pool.get('stock.move')
        Period = pool.get('account.period')
        invoice = self.invoice
        result = super(InvoiceLine, self).get_move_lines()
        product = self.product
        if self.type != 'line' or not product or product.type != 'goods' or product.consumable is True:
            return result

        accounting_date = (invoice.accounting_date or invoice.invoice_date)
        period_id = Period.find(invoice.company.id, date=accounting_date)
        period = Period(period_id)
        account_stock_method = period.fiscalyear.account_stock_method
        if account_stock_method != 'latin':
            return result

        if invoice.type == 'in':
            # result = self._temporal_fix(result)
            return result

        moves = []
        # for move in self.stock_moves:
        #     if move.state != 'done':
        #         continue
        #
        #     # remove move for different product
        #     if move.product != product:
        #         raise UserWarning(gettext(
        #             'account_stock_latin.msg_invoice_line_stock_move_different_product'
        #         ))
        #     else:
        #         moves.append(move)

        type_ = 'out_customer'
        if self.quantity < 0:
            direction, target = type_.split('_')
            if direction == 'in':
                direction = 'out'
            else:
                direction = 'in'
            type_ = '%s_%s' % (direction, target)

        moves.sort(key=operator.attrgetter('effective_date'))
        cost = Move.update_latin_quantity_product_cost(
            product, moves, abs(self.quantity), self.unit, type_)
        cost = invoice.currency.round(cost)

        with Transaction().set_context(date=accounting_date):
            latin_move_lines = self._get_latin_move_lines(
                cost, type_, account_stock_method)
        result.extend(latin_move_lines)
        return result
